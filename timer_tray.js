const { Tray, Menu, app } = require("electron");

class TimerTray extends Tray {
  constructor(mainWindow, iconPath) {
    super(iconPath);

    this.mainWindow = mainWindow;
    this.setToolTip("Toggle Timer App");

    this.on("click", this.onClick);
    this.on("right-click", this.onRightClick);
  }

  onClick = () => {
    if (this.mainWindow.isVisible()) {
      this.mainWindow.hide();
    } else {
      const { width, height } = this.mainWindow.getBounds();
      const trayElementBounds = this.getBounds();
      const horisontalBase = trayElementBounds.y - 10;
      const verticalBase = trayElementBounds.x + trayElementBounds.width / 2;

      this.mainWindow.setBounds({
        x: verticalBase - width / 2,
        y: horisontalBase - height,
        width,
        height,
      });

      this.mainWindow.show();
    }
  }

  onRightClick = () => {
    const menuCofig = Menu.buildFromTemplate([
      { label: "Quit", click: () => app.quit() },
    ]);

    this.popUpContextMenu(menuCofig);
  }
}

module.exports = TimerTray;
