const { ipcRenderer, contextBridge } = require("electron");

contextBridge.exposeInMainWorld("api", {
  updateTimer: (title) => ipcRenderer.send("update_timer", title),
});
