import { Fragment } from "react";
import { Link, Outlet } from "react-router-dom";

const styles = {
  container: {
    height: "88vh",
  },
};

const Header = () => {
  return (
    <Fragment>
      <nav>
        <div className="nav-wrapper">
          <Link
            to="/settings"
            className="brand-logo right"
            style={{ cursor: "pointer" }}
          >
            <i className="material-icons">settings</i>
          </Link>
          <ul>
            <li>
              <Link to="/">Active Task</Link>
            </li>
            <li>
              <Link to="/tasks">All Tasks</Link>
            </li>
          </ul>
        </div>
      </nav>

      <div className="container" style={styles.container}>
        <Outlet />
      </div>
    </Fragment>
  );
};

export default Header;
