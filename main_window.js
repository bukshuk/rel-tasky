const { BrowserWindow } = require("electron");

class MainWindow extends BrowserWindow {
  constructor(config, url) {
    super(config);

    this.loadURL(url);
    this.on("blur", this.onBlur);
  }

  onBlur() {
    this.hide();
  }
}

module.exports = MainWindow;
