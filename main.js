const path = require("path");
const { app, ipcMain } = require("electron");

const MainWindow = require("./main_window");
const TimerTray = require("./timer_tray");

let mainWindow;
let timerTray;

app.on("ready", () => {
  const mainWindowConfig = {
    height: 500,
    width: 300,
    frame: false,
    resizable: false,
    show: false,
    skipTaskbar: true,
    webPreferences: {
      sandbox: true,
      preload: path.join(__dirname, "preload.js"),
      backgroundThrottling: false,
    },
  };
  const url = "http://localhost:3000/";
  mainWindow = new MainWindow(mainWindowConfig, url);

  const iconName =
    process.platform === "win32" ? "windows-icon.png" : "iconTemplate.png";
  const iconPath = path.join(__dirname, "src", "assets", iconName);
  timerTray = new TimerTray(mainWindow, iconPath);
});

ipcMain.on("update_timer", (_, timeLeft) => {
  console.log(timeLeft);
  timerTray.setTitle(timeLeft)
});
